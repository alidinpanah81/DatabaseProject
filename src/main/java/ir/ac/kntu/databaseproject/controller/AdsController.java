package ir.ac.kntu.databaseproject.controller;

import ir.ac.kntu.databaseproject.model.ads.Ads;
import ir.ac.kntu.databaseproject.service.AdsService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/ads")
public class AdsController {

    @Autowired
    private AdsService adsService;

    @PostMapping("/create")
    public ResponseEntity<Ads> createAds(@RequestBody @Valid Ads ads) {
        Ads createdAds = adsService.createAds(ads);
        return new ResponseEntity<>(createdAds, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<Ads>> getAllAds(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int number) {
        Page<Ads> adsPage = adsService.getAllAds(page, number);
        return ResponseEntity.ok(adsPage);
    }

    @GetMapping("/{adsId}")
    public ResponseEntity<Ads> getAdsById(@PathVariable Long adsId) {
        Optional<Ads> ads = adsService.getAdsById(adsId);
        return ads.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/update/{adsId}")
    public ResponseEntity<Ads> updateAds(@PathVariable Long adsId, @RequestBody @Valid Ads updatedAds) {
        return ResponseEntity.ok(adsService.updateAds(adsId, updatedAds));
    }

    @DeleteMapping("/delete/{adsId}")
    public ResponseEntity<Void> deleteAds(@PathVariable Long adsId) {
        adsService.deleteAds(adsId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/search")
    public ResponseEntity<Page<Ads>> searchByCityOrState(
            @RequestParam String searchTerm,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int number) {
        Page<Ads> searchResult = adsService.searchByCityOrState(searchTerm, (Pageable) PageRequest.of(page, number));
        return ResponseEntity.ok(searchResult);
    }

    @GetMapping("/filter")
    public ResponseEntity<Page<Ads>> filterAds(@RequestParam(required = false) Double minPrice,
                                               @RequestParam(required = false) Double maxPrice,
                                               @RequestParam(required = false) Integer minSize,
                                               @RequestParam(required = false) Integer maxSize,
                                               @RequestParam(required = false) String type,
                                               @RequestParam(required = false) Integer roomNumber,
                                               @RequestParam(required = false) List<String> conditions,
                                               @RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "10") int number) {
        Page<Ads> filteredAds = adsService.filterAds(minPrice, maxPrice, minSize, maxSize, type, roomNumber, conditions, (Pageable) PageRequest.of(page, number));
        return ResponseEntity.ok(filteredAds);
    }
}

