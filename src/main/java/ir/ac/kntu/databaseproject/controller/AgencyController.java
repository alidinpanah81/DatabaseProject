package ir.ac.kntu.databaseproject.controller;

import ir.ac.kntu.databaseproject.model.Agency;
import ir.ac.kntu.databaseproject.service.AgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/agencies")
public class AgencyController {

    @Autowired
    private AgencyService agencyService;

    @PostMapping("/create")
    public ResponseEntity<Agency> createAgency(@RequestBody Agency agency) {
        Agency createdAgency = agencyService.createAgency(agency);
        return ResponseEntity.created(URI.create("/api/agencies/" + createdAgency.getId())).body(createdAgency);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<Agency>> getAllAgencies(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        PageRequest pageable = PageRequest.of(page, size);
        Page<Agency> agenciesPage = agencyService.getAllAgencies(page, size);
        return ResponseEntity.ok(agenciesPage);
    }

    @GetMapping("/{agencyId}")
    public ResponseEntity<Agency> getAgencyById(@PathVariable Long agencyId) {
        Optional<Agency> agency = agencyService.getAgencyById(agencyId);
        return agency.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{agencyId}/update")
    public ResponseEntity<Agency> updateAgency(@PathVariable Long agencyId, @RequestBody Agency updatedAgency) {
        return ResponseEntity.ok(agencyService.updateAgency(agencyId, updatedAgency));
    }

    @DeleteMapping("/{agencyId}/delete")
    public ResponseEntity<Void> deleteAgency(@PathVariable Long agencyId) {
        agencyService.deleteAgency(agencyId);
        return ResponseEntity.noContent().build();
    }
}
