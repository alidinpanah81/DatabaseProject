package ir.ac.kntu.databaseproject.model.ads;

public enum Type {
    RESIDENTIAL, COMMERCIAL, OFFICIAL, INDUSTRIAL

}
