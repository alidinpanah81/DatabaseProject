package ir.ac.kntu.databaseproject.model;

import ir.ac.kntu.databaseproject.model.ads.Ads;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Agency {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "agency")
    @Column(name = "ads")
    private List<Ads> ads;

    @Column(name = "agencyName",nullable = false)
    private String agencyName;

    @Column(name = "city",nullable = false)
    private String city;

    @Column(name = "bossFirstName",nullable = false)
    private String bossFirstName;

    @Column(name = "bossLastName",nullable = false)
    private String bossLastName;

    @Column(name = "bossPhoneNumber", nullable = false)
    @Pattern(regexp = "^09\\d{9}$")
    private String bossPhoneNumber;

    @Column(name = "password",nullable = false)
    private String password;

    @Column(name = "agencyNumber")
    private String agencyNumber;

    @Column(name = "employeeNumber")
    private int employeeNumber;

}
