package ir.ac.kntu.databaseproject.model.ads;

import ir.ac.kntu.databaseproject.model.Agency;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Ads {

    @Id
    @GeneratedValue
    private Long id;

    private String uuid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agency_id")
    private Agency agency;

    @Lob
    private byte[] images;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "type", nullable = false)
    private Type type;

    @Column(name = "roomNumber")
    private int roomNumber;

    @Column(name = "conditions")
    @ElementCollection(targetClass = Conditions.class)
    @Enumerated(EnumType.STRING)
    List<Conditions> conditions;

    @Column(name = "facilities")
    @ElementCollection(targetClass = Facilities.class)
    @Enumerated(EnumType.STRING)
    List<Facilities> facilities;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;

    @Column(name = "price")
    private double price;

    @Column(name = "rent")
    private double rent;

    @Column(name = "mortgage")
    private double mortgage;

    @Column(name = "size")
    private int size;

    @Column(name = "state")
    private State state;

}
