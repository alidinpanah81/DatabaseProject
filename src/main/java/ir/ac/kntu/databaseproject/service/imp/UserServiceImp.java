package ir.ac.kntu.databaseproject.service.imp;

import ir.ac.kntu.databaseproject.model.User;
import ir.ac.kntu.databaseproject.repository.UserRepository;
import ir.ac.kntu.databaseproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SmsServiceImp smsServiceImp;

    public User createUser(User user) {

        return userRepository.save(user);
    }

    public Page<User> getAllUsers(int page, int number) {
        PageRequest pageable = PageRequest.of(page, number);
        return userRepository.findAll(pageable);
    }

    public Optional<User> getUserById(Long userId) {
        return userRepository.findById(userId);
    }

    public User updateUser(Long userId, User updatedUser) {
        Optional<User> existingUser = userRepository.findById(userId);
        if (existingUser.isPresent()) {
            User userToUpdate = existingUser.get();
            userToUpdate.setPhoneNumber(updatedUser.getPhoneNumber());
            userToUpdate.setEmail(updatedUser.getEmail());
            userToUpdate.setPassword(updatedUser.getPassword());
            userToUpdate.setFirstName(updatedUser.getFirstName());
            userToUpdate.setLastName(updatedUser.getLastName());
            return userRepository.save(userToUpdate);
        } else {
            throw new RuntimeException("User not found with id: " + userId);
        }
    }

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    public void sendVerificationCode(String phoneNumber) {
        String verificationCode = generateRandomCode();

        User user = new User();
        if (userRepository.findByPhoneNumber(phoneNumber).isPresent()) {
            user = userRepository.findByPhoneNumber(phoneNumber).get();
        } else {
            user.setPhoneNumber(phoneNumber);
        }
        user.setVerificationCode(verificationCode);
        userRepository.save(user);

        smsServiceImp.sendVerificationCode(phoneNumber, verificationCode);
    }

    public boolean verifyCode(String phoneNumber, String enteredCode) {
        Optional<User> optionalUser = userRepository.findByPhoneNumber(phoneNumber);

        return optionalUser.get().getVerificationCode().equals(enteredCode);
    }

    private String generateRandomCode() {
        Random random = new Random();
        int code = 100000 + random.nextInt(900000); // Generate a random 6-digit code
        return String.valueOf(code);
    }

}
