package ir.ac.kntu.databaseproject.service;

public interface SmsService {
    void sendVerificationCode(String phoneNumber, String verificationCode);
}
