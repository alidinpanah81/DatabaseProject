package ir.ac.kntu.databaseproject.service.imp;

import ir.ac.kntu.databaseproject.model.Agency;
import ir.ac.kntu.databaseproject.repository.AgencyRepository;
import ir.ac.kntu.databaseproject.service.AgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgencyServiceImpl implements AgencyService {

    @Autowired
    private AgencyRepository agencyRepository;


    public Agency createAgency(Agency agency) {
        return agencyRepository.save(agency);
    }


    public Page<Agency> getAllAgencies(int page, int number) {
        PageRequest pageable= PageRequest.of(page,number);
        return agencyRepository.findAll(pageable);
    }


    public Optional<Agency> getAgencyById(Long agencyId) {
        return agencyRepository.findById(agencyId);
    }


    public Agency updateAgency(Long agencyId, Agency updatedAgency) {
        Optional<Agency> existingAgency = agencyRepository.findById(agencyId);
        if (existingAgency.isPresent()) {
            Agency agencyToUpdate = existingAgency.get();
            agencyToUpdate.setAds(updatedAgency.getAds());
            agencyToUpdate.setAgencyName(updatedAgency.getAgencyName());
            agencyToUpdate.setCity(updatedAgency.getCity());
            agencyToUpdate.setBossFirstName(updatedAgency.getBossFirstName());
            agencyToUpdate.setBossLastName(updatedAgency.getBossLastName());
            agencyToUpdate.setBossPhoneNumber(updatedAgency.getBossPhoneNumber());
            agencyToUpdate.setPassword(updatedAgency.getPassword());
            agencyToUpdate.setAgencyNumber(updatedAgency.getAgencyNumber());
            agencyToUpdate.setEmployeeNumber(updatedAgency.getEmployeeNumber());
            return agencyRepository.save(agencyToUpdate);
        } else {
            throw new RuntimeException("Agency not found with id: " + agencyId);
        }
    }


    public void deleteAgency(Long agencyId) {
        agencyRepository.deleteById(agencyId);
    }

}
