package ir.ac.kntu.databaseproject.service;

import ir.ac.kntu.databaseproject.model.ads.Ads;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

public interface AdsService {

    Ads createAds(Ads ads);

    Page<Ads> getAllAds(int page, int number);

    Optional<Ads> getAdsById(Long adsId);

    Ads updateAds(Long adsId, Ads updatedAds);

    void deleteAds(Long adsId);

    Page<Ads> searchByCityOrState(String searchTerm, Pageable pageable);

    Page<Ads> filterAds(Double minPrice, Double maxPrice, Integer minSize, Integer maxSize,
                        String type, Integer roomNumber, List<String> conditions, Pageable pageable);
}
