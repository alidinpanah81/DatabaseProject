package ir.ac.kntu.databaseproject.service;

import ir.ac.kntu.databaseproject.model.Agency;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface AgencyService {
    Agency createAgency(Agency agency);

    Page<Agency> getAllAgencies(int page, int number);

    Optional<Agency> getAgencyById(Long agencyId);

    Agency updateAgency(Long agencyId, Agency updatedAgency);

    void deleteAgency(Long agencyId);
}
