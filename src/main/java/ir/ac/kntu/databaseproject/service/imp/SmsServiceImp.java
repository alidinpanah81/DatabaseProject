package ir.ac.kntu.databaseproject.service.imp;

import ir.ac.kntu.databaseproject.service.SmsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SmsServiceImp implements SmsService {

    @Value("${sms.apiKey}") // Add your SMS service API key to application.properties or application.yml
    private String apiKey;

    @Value("${sms.apiUrl}") // Add your SMS service API URL to application.properties or application.yml
    private String apiUrl;

    public void sendVerificationCode(String phoneNumber, String verificationCode) {
        // You'll need to replace the following code with the actual implementation
        // provided by your SMS service provider

        // Assuming you have an API that accepts phone number and verification code
        // and sends an SMS, make an HTTP request to the SMS service API

        String smsApiUrl = apiUrl + "?apiKey=" + apiKey + "&phoneNumber=" + phoneNumber + "&verificationCode=" + verificationCode;

        // Use RestTemplate or another HTTP client to make the request
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(smsApiUrl, String.class);

        // You may want to handle the response from the SMS service (success, failure, etc.)
        System.out.println("SMS Service Response: " + response);
    }
}
