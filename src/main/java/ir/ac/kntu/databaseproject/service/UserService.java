package ir.ac.kntu.databaseproject.service;

import ir.ac.kntu.databaseproject.model.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface UserService {
    User createUser(User user);

    Page<User> getAllUsers(int page, int number);

    Optional<User> getUserById(Long userId);

    User updateUser(Long userId, User updatedUser);

    void deleteUser(Long userId);

    void sendVerificationCode(String phoneNumber);

    boolean verifyCode(String phoneNumber, String enteredCode);

}
