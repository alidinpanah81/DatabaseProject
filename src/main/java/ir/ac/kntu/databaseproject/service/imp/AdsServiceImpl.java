package ir.ac.kntu.databaseproject.service.imp;

import ir.ac.kntu.databaseproject.model.ads.Ads;
import ir.ac.kntu.databaseproject.repository.AdsRepository;
import ir.ac.kntu.databaseproject.service.AdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.awt.print.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdsServiceImpl implements AdsService {

    @Autowired
    private AdsRepository adsRepository;

    public Ads createAds(Ads ads) {
        return adsRepository.save(ads);

    }

    public Page<Ads> getAllAds(int page, int number) {
        PageRequest pageable = PageRequest.of(page, number);
        return adsRepository.findAll(pageable);
    }

    public Optional<Ads> getAdsById(Long adsId) {
        return adsRepository.findById(adsId);
    }

    public Ads updateAds(Long adsId, Ads updatedAds) {
        Optional<Ads> existingAds = adsRepository.findById(adsId);
        if (existingAds.isPresent()) {
            Ads adsToUpdate = existingAds.get();
            // Update other properties as needed
            adsToUpdate.setTitle(updatedAds.getTitle());
            adsToUpdate.setDescription(updatedAds.getDescription());
            adsToUpdate.setType(updatedAds.getType());
            adsToUpdate.setRoomNumber(updatedAds.getRoomNumber());
            adsToUpdate.setConditions(updatedAds.getConditions());
            adsToUpdate.setFacilities(updatedAds.getFacilities());
            adsToUpdate.setCity(updatedAds.getCity());
            adsToUpdate.setDistrict(updatedAds.getDistrict());
            adsToUpdate.setPrice(updatedAds.getPrice());
            adsToUpdate.setRent(updatedAds.getRent());
            adsToUpdate.setMortgage(updatedAds.getMortgage());
            adsToUpdate.setSize(updatedAds.getSize());
            adsToUpdate.setState(updatedAds.getState());
            return adsRepository.save(adsToUpdate);
        } else {
            throw new RuntimeException("Ads not found with id: " + adsId);
        }
    }

    public void deleteAds(Long adsId) {
        adsRepository.deleteById(adsId);
    }

    public Page<Ads> searchByCityOrState(String searchTerm, Pageable pageable) {
        return adsRepository.findByCityContainingIgnoreCaseOrStateContainingIgnoreCase(searchTerm, searchTerm, pageable);
    }

    public Page<Ads> filterAds(Double minPrice, Double maxPrice, Integer minSize, Integer maxSize,
                               String type, Integer roomNumber, List<String> conditions, Pageable pageable) {
        return adsRepository.filterAds(minPrice, maxPrice, minSize, maxSize, type, roomNumber, conditions, pageable);
    }

}
