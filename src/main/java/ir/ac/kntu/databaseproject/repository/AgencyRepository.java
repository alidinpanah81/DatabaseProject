package ir.ac.kntu.databaseproject.repository;

import ir.ac.kntu.databaseproject.model.Agency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgencyRepository extends JpaRepository<Agency,Long> {
}
