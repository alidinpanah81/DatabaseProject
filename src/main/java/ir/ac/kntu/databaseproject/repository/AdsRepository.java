package ir.ac.kntu.databaseproject.repository;

import ir.ac.kntu.databaseproject.model.ads.Ads;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.awt.print.Pageable;
import java.util.List;

public interface AdsRepository extends JpaRepository<Ads,Long> {

    Page<Ads> findByCityContainingIgnoreCaseOrStateContainingIgnoreCase(String city, String state, Pageable pageable);

    @Query("SELECT a FROM Ads a " +
            "WHERE (:minPrice IS NULL OR a.price >= :minPrice) " +
            "AND (:maxPrice IS NULL OR a.price <= :maxPrice) " +
            "AND (:minSize IS NULL OR a.size >= :minSize) " +
            "AND (:maxSize IS NULL OR a.size <= :maxSize) " +
            "AND (:type IS NULL OR a.type = :type) " +
            "AND (:roomNumber IS NULL OR a.roomNumber = :roomNumber) " +
            "AND (:conditions IS NULL OR a.conditions IN :conditions)")
    Page<Ads> filterAds(Double minPrice, Double maxPrice, Integer minSize, Integer maxSize,
                        String type, Integer roomNumber, List<String> conditions, Pageable pageable);
}
